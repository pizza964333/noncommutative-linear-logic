
import System.Random
import Control.Applicative
import Control.Monad
import System.IO.Unsafe
import System.Random.Shuffle
import Data.List
import Data.Maybe
import Control.Monad.State.Lazy
import Control.Monad.Trans.Maybe
import System.Timeout

type Marker = Integer

data Term = MCU
          | I Term
          | T Integer
          | Term :* Term
          | Term :~ Term
          | Hide Marker Term
          | OFC Term
          | Empty
 deriving (Show,Eq)

type MarkerBefore = Marker
type MarkerAfter  = Marker
type MCheckElem = ([MarkerBefore],Marker,[MarkerAfter])
type MCheckList = [(Marker,([MarkerBefore],[MarkerAfter]))]

type MCheck a = StateT MCheckList (MaybeT IO) a

collectState a = unsafePerformIO $ do
  --b <- mapM (\_ -> permute a) [0..12]
  b <- mapM (\_ -> permute a) [0..1]
  let c = map collectState0 b
  return $ mergeMcst $ foldr1 (++) c
  

{-
collectState :: Term -> MCheckList
collectState (a@(b :~ c) :* d) = mergeMcst ( colst b c ++ collectState d )
collectState (a :* b@(c :~ d)) = mergeMcst ( collectState a ++ colst c d )
collectState (a :* b) = mergeMcst ( collectState a ++ collectState b )
collectState _ = []
-}

collectState0 :: Term -> MCheckList
collectState0 (a@(b :~ c) :* d) = mergeMcst ( colst b c ++ collectState d )
collectState0 (a :* b@(c :~ d)) = mergeMcst ( collectState a ++ colst c d )
collectState0 (a :* b) = mergeMcst ( collectState a ++ collectState b )
collectState0 _ = []

colst a o@(b :~ c) = mergeMcst k
 where
  e = msq2c a
  g = msq2c c
  q = msq2c o
  w = msq2c (a :~ b)
  h = collectState b
  k = (e `before` h) ++ (h `before` g) ++ (e `before` g) ++ (e `before` p) -- ++ (e `before` q) ++ (w `before` g)
  p = colst b c

colst a b = e
 where
  c = msq2c a
  d = msq2c b
  e = c `before` d

before [] [] = []
before [] a = a
before a [] = a
before a b = mergeMcst ( c ++ d )
 where
  c = concatMap (\(d,(e,f)) -> map (aft d e f) b) a
  d = concatMap (\(d,(e,f)) -> map (bef d e f) a) b
  aft a b c (d,(e,f)) = (a,(b,nub $ c++[d]++f))
  bef a b c (d,(e,f)) = (a,(nub $ b++[d]++e,c))

mergeMcst :: MCheckList -> MCheckList
mergeMcst [] = []
mergeMcst a = expandMergeMcst c
 where
  b = groupBy (\(a,_) (b,_) -> a == b) $ sortBy (\(a,_) (b,_) -> compare a b) a
  c = map (foldr1 (\(a,(b,c)) (d,(e,f)) -> (a,(nub $ b++e,nub $ c++f)))) b

expandMergeMcst a = map (\(b,(c,d)) -> (b,(ebef c,eaft d))) a
 where
  ebef b = nub $ concatMap (\b -> [b] ++ (fb $ lookup b a)) b
  eaft b = nub $ concatMap (\b -> [b] ++ (fa $ lookup b a)) b
  fb Nothing = []
  fb (Just (a,b)) = a
  fa Nothing = []
  fa (Just (a,b)) = b

msq2c = mseqToMCheckList

mseqToMCheckList a@(_ :~ _) = h
 where
  e = map getMarker $ filter isHide $ mseqToList a
  g = length e
  h = map (\n -> f $ splitAt n e) [0..(g-1)]
  f (a,b) = (head b,(a,tail b))
mseqToMCheckList (Hide a b) = [(a,([],[]))]
mseqToMCheckList _ = []

annthMseq :: Term -> MCheck [(Term,[Marker],[Marker])]
annthMseq a@(_ :~ _) = do
  s <- get
  let e = map (expand s) d
  return e
 where
  b = zip (mseqToList a) [0..]
  c = map (\(a,b) -> (getMarker a,b)) $ filter (isHide.fst) b
  d = map (\(a,b) -> (a,map fst $ filter (\(c,d) -> d < b) c, map fst $ filter (\(c,d) -> d > b) c) ) b
  expand s (a,b,c) = (a,ebef b, eaft c)
   where
    ebef b = nub $ concatMap (\b -> [b] ++ (fb $ lookup b s)) b
    eaft b = nub $ concatMap (\b -> [b] ++ (fa $ lookup b s)) b
    fb Nothing = []
    fb (Just (a,b)) = a
    fa Nothing = []
    fa (Just (a,b)) = b

{-
updateMCheckState :: MCheckElem -> MCheck ()
updateMCheckState (a,b,c) = do
  st1 <- get
  let st2 = nubBy (\(a,_) (b,_) -> a == b) $ map (\(m,(mb,ma)) -> update m st1 mb ma) g ++ st1
  put st2
 where
  d = a ++ [b] ++ c
  e = length d
  g = map (\n -> f $ splitAt n d) [0..(e-1)]
  f (a,b) = (head b,(a,tail b))
  update m st1 mb ma = case lookup m st1 of
    Nothing -> (m,(mb,ma))
    Just (mb0,ma0) -> (m,(nub $ mb0 ++ mb, nub $ ma0 ++ ma))
-}

runMCheck :: MCheck a -> IO (Maybe (a,MCheckList))
runMCheck a = runMaybeT (runStateT a [])

liftMCheck :: MCheck Term -> MCheck (Maybe Term)
liftMCheck a = do
  s <- get
  b <- liftIO $ runMaybeT (runStateT a s)
  case b of
    Nothing -> return Nothing
    Just (b,s) -> put s >> return (Just b)

liftMCheckTimeout :: Int -> MCheck Term -> MCheck (Maybe (Maybe Term))
liftMCheckTimeout t a = do
  s <- get
  b <- liftIO $ timeout t $ runMaybeT (runStateT a s)
  case b of
    Nothing -> return Nothing
    (Just Nothing) -> return $ Just Nothing
    (Just (Just (b,s))) -> put s >> return (Just (Just b))

liftMCheckIL :: MCheck Term -> MCheck (Maybe Term)
liftMCheckIL a = do
  s <- get
  b <- liftIO $ unsafeInterleaveIO $ runMaybeT (runStateT a s)
  case b of
    Nothing -> return Nothing
    Just (b,s) -> put s >> return (Just b)

invert :: Term -> Term
invert (T a) = I (T a)
invert (I a) = a
invert (a :* b) = invert a :* invert b
invert (a :~ b) = invert a :~ invert b
invert (OFC a) = OFC $ invert a
invert (MCU) = MCU
invert (Hide a b) = Hide a (invert b)

randomMarker :: IO Marker
randomMarker = do
  a <- randomRIO (10^9,10^10)
  return a

rewrite :: Term -> MCheck Term

rewrite (MCU :* a) = return a
rewrite (a :* MCU) = return a
rewrite (MCU :~ a) = return a
rewrite (a :~ MCU) = return a


rewrite o@(OFC a) = do
  b <- liftIO $ randomRIO (0,99999 :: Int)
  c <- liftIO $ randomRIO (0,99999 :: Int)
  g <- liftIO $ randomRIO (0,99999 :: Int)
  let d = (b+c+1)*(b+c)`div`2+b
  let e = (d + g) `mod` 12
  case e of
   0 -> mzero
   1 -> mzero
   2 -> mzero
   3 -> mzero
   4 -> mzero
   5 -> mzero
   6 -> mzero
   7 -> mzero
   8 -> mzero
   9 -> return MCU
   _ -> return (o :* a)

rewrite (T _) = mzero
rewrite (I (T _)) = mzero
rewrite (Hide _ _) = mzero

rewrite (I (I a)) = return a

rewrite (a@(Hide b c) :* d) = do
  e <- liftMCheck $ debugRewrite d
  case e of
    Nothing -> mzero
    Just e  -> return (a :* e)

rewrite (a :* b@(Hide c d)) = debugRewrite (b :* a)

rewrite (I (T a) :* b) = do
  c <- liftMCheck $ debugRewrite $ (T a :* invert b)
  case c of
    Nothing -> mzero
    Just c  -> return $ invert c


rewrite (T a :* b) = do
  m <- liftIO $ randomMarker
  c <- debugFindIPF ([],m,[]) (T a) b
  let d = length c
  fun d m c a b
 where
  fun 0 m c a b = fRewrite b (\b -> T a :* b)
  fun d m c a b = do e <- liftIO $ randomRIO (0,d-1)
                     let f = (c !! e) b
                     return $ (Hide m (T a) :* f)

rewrite (a :* b@(I (T c))) = debugRewrite (b :* a)

rewrite (a :* T b) = debugRewrite (T b :* a)

rewrite (a@(b :~ c) :* d) = do
  sel <- liftIO $ randomAtomSelect a
  let Just (e,f,p) = sel
  --(if isJust sel then updateMCheckState p else return ())
  let (_,m,_) = p
  case (isJust sel) of
    False -> fRewrite d (\d -> a :* d)
    True -> do
      g <- debugFindIPF p e d
      let h = length g
      case (h > 0) of
              True  -> do
                    i <- liftIO $ randomRIO (0,h-1)
                    let j = (g !! i) d
                    return (f :* j)
              False -> do
                    i <- liftMCheck $ fRewrite d (\d -> a :* d)
                    j <- liftIO $ debugPermute (f :* d)
                    k <- liftMCheck $ debugRewrite j
                    maybeToMonad (liftM (unhide m) k <|> i)

rewrite (a :* b@(c :~ d)) = debugRewrite (b :* a)

rewrite (a :~ b) = do
  c <- liftMCheck $ debugRewrite a
  d <- liftMCheck $ debugRewrite b
  case (c,d) of
    (Nothing, Nothing) -> mzero
    (Just a,  Nothing) -> fun (a :~ b)
    (Nothing,  Just b) -> fun (a :~ b)
    (Just a,   Just b) -> fun (a :~ b)
 where
  fun (a :~ b) = do c <- liftMCheck $ debugRewrite (a :~ b)
                    case c of
                      (Just c) -> return c
                      Nothing  -> return (a :~ b)

rewrite (a :* b) = do
  c <- liftMCheck $ debugRewrite a
  d <- liftMCheck $ debugRewrite b
  case (c,d) of
    (Nothing, Nothing) -> mzero
    (Just a,  Nothing) -> fun (a :* b)
    (Nothing,  Just b) -> fun (a :* b)
    (Just a,   Just b) -> fun (a :* b)
 where
  fun (a :* b) = do c <- liftMCheck $ debugRewrite (a :* b)
                    d <- liftMCheck $ debugRewrite (b :* a)
                    case (c,d) of
                      (Just c,_) -> return c
                      (_,Just d) -> return d
                      (Nothing,Nothing)  -> return (a :* b)

rewrite (I a) = do
  b <- liftMCheck $ debugRewrite a
  case b of
    Nothing    -> return $ invert a
    Just  b    -> do c <- liftMCheck $ debugRewrite (I b)
                     case c of
                        Nothing -> return (I b)
                        Just  c -> return c


fRewrite a f = do
  b <- liftMCheck $ debugRewrite a
  case b of
    Nothing -> mzero
    Just  b -> return $ f b

debugRewrite a = do
  b <- rewrite a
  --print ("debugRewrite",a)
  return b

debugPermute a = do
  b <- permute a
  --print ("debugPermute",a)
  return b

permute :: Term -> IO Term
permute a@(b :* c) = do
  let d = mcnjToList a
  e <- shuffleM d
  f <- mapM permute e
  return $ listToMcnj f
  

permute (a :~ b) = do
  c <- debugPermute a
  d <- debugPermute b
  return (c :~ d)

permute (I a) = do
  b <- debugPermute a
  return (I b)

permute a = return a

mcnjToList (a :* b) = mcnjToList a ++ mcnjToList b
mcnjToList a = [a]

listToMcnj [a] = a
listToMcnj (a:b) = a :* listToMcnj b


mseqToList (a :~ b) = mseqToList a ++ mseqToList b
mseqToList a = [a]

listToMseq [] = Empty
listToMseq [a] = a
listToMseq (a:b) = a :~ listToMseq b

-- marker intersection checking or markers before current marker and markers after
-- find inverted polarity atom in term accoring to marker intersection rules
-- first arguments is maskers
-- second is atom to find inverted polarity
-- third is term structure
-- returns list of functions each maps term structure to structure with polar atom hidden with current marker
findIPF :: ([Marker],Marker,[Marker]) -> Term -> Term -> MCheck [Term -> Term]

findIPF ([],m,[]) (T a) b@(I (T c)) | a == c = return [(\_ -> Hide m b)]

--findIPF o@([],m,[]) a@(T b) (c :* d) = funMap (findIPF o a c) (\c -> c :* d) ++ funMap (findIPF o a d) (\d -> c :* d)

findIPF o@(_,_,_) a@(T b) (c :* d) = lifp funMap (findIPF o a c) (\c -> c :* d) +++ lifp funMap (findIPF o a d) (\d -> c :* d)

findIPF o@([],m,[]) a@(T b) (c :~ d) = lifp funMap (findIPF o a c) (\c -> c :~ d) +++ lifp funMap (findIPF o a d) (\d -> c :~ d)

--findIPF o@([],m,[]) a@(T b) (I c)    = funMap (findIPF o a c) (\c -> I c)

findIPF o@(_,_,_) a@(T b) (I c)    = lifp funMap (findIPF o a c) (\c -> I c)

findIPF o@(mb,m,ma) a@(T b) c@(d :~ e) = do
  g <- annthMseq c
  let h = dropWhile (\(k,p,q) -> crossElem mb q) g
  let bh = listToMseq $ map f $ takeWhile (\(k,p,q) -> crossElem mb q) g
  let j = dropWhile (\(k,p,q) -> crossElem ma p) $ reverse h
  let aj = listToMseq $ map f $ reverse $ takeWhile (\(k,p,q) -> crossElem ma p) $ reverse h
  let window = listToMseq $ map f $ reverse j
  --liftIO $ print ("window",a,window,g)
  case length j > 0 of
    True -> do p <- findIPF ([],m,[]) a window
               let q = funMap p (\p -> reduceEmpty (bh :~ p :~ aj))
               return q
    False -> return []
 where
  f (a,b,c) = a

findIPF a b c = return []

{-
  case atomElem 9 b && atomElem 9 c of
     False -> return []
     True  -> liftIO (print ("kuku",a,b,c)) >> return []
-}

reduceEmpty (Empty :~ a :~ Empty) = a
reduceEmpty (Empty :~ a :~ b) = a :~ b
reduceEmpty (a :~ b :~ Empty) = a :~ b
reduceEmpty a = a

crossElem a b = or $ map (\b -> elem b a) b

lifp f a c = do
  b <- a
  return $ f b c

a +++ b = do
  c <- a
  b <- b
  return (c++b)

atomElem n (T a) | a == n = True
atomElem n (I (T a)) | a == n = True
atomElem n (a :* b) = atomElem n a || atomElem n b
atomElem n (a :~ b) = atomElem n a || atomElem n b
atomElem n (I a) = atomElem n a
atomElem _ _ = False

funMap f j = map (\f -> j . f) f

debugFindIPF a b c = do
  d <- findIPF a b c
  --print ("debugFindIPF", a, b, c, length d)
  return d

-- concat sequence of terms on input
-- atom on output
-- concat sequence of term with this atom hidden
-- (hide markers before, hide marker, hide markers after)
randomAtomSelect :: Term -> IO (Maybe (Term,Term,([Marker],Marker,[Marker])))
randomAtomSelect a
 | e == 0 = return Nothing
 | otherwise = do
  n <- randomRIO (0,e-1)
  let g@(q,p) = c !! n
  m <- randomMarker
  let h = g `delete` c
  let hd = (Hide m q, p)
  let k = map fst $ sortSnd ( (hd:h) ++ d )
  let j = map (\(a,b) -> (getMarker a,b)) $ filter (isHide.fst) $ sortSnd (hd:d)
  let (r,s:t) = tmap (map fst) $ break ((==p).snd) j
  return $ Just (q,listToMseq k,(r,s,t))
 where
  b = mseqToList a
  c = filter (isAtom.fst)     $ zip b [0..]
  d = filter (not.isAtom.fst) $ zip b [0..]
  e = length c

sortSnd = sortBy (\(a,b) (c,d) -> compare b d)

isHide (Hide _ _) = True
isHide _ = False

getMarker (Hide m _) = m

tmap f (a,b) = (f a, f b)

isAtom (T _) = True
isAtom (I (T _)) = True
isAtom _ = False

unhide m (Hide a b) | a == m = b
unhide m (a :* b) = unhide m a :* unhide m b
unhide m (a :~ b) = unhide m a :~ unhide m b
unhide m (I a)    = I $ unhide m a
unhide m a = a

pretty MCU = "MCU"
pretty (OFC a) = "[! " ++ pretty a ++ " !]"
pretty (T a) = "⊤" ++ show a
pretty (I (T a)) = "⊥" ++ show a
pretty (a@(_ :~ _) :* b@(_ :~ _)) = "( " ++ pretty a ++ " ) * ( " ++ pretty b ++ " )"
pretty (o@(a :~ b) :* c) = "( " ++ pretty o ++ " ) * " ++ pretty c
pretty (a :* o@(b :~ c)) = pretty a ++ " * ( " ++ pretty o ++ " )"
pretty (a :* b) = pretty a ++ " * " ++ pretty b
pretty (a :~ b) = pretty a ++ " ~ " ++ pretty b
pretty (I a) = pretty $ invert a
pretty (Hide _ a) = "{" ++ filter (/=' ') (pretty a) ++ "}"
pretty Empty = "Empty"
--pretty a = error (show a)

removeHides (Hide _ _ :* Hide _ _) = Empty
removeHides (Hide _ _ :~ Hide _ _) = Empty
removeHides (Hide _ _ :* a) = removeHides a
removeHides (a :* Hide _ _) = removeHides a
removeHides (a :~ Hide _ _) = removeHides a
removeHides (Hide _ _ :~ a) = removeHides a
removeHides (a :* b) = remEm $ removeHides a :* removeHides b
removeHides (a :~ b) = remEm $ removeHides a :~ removeHides b
removeHides (I a) = I $ removeHides a
removeHides a = a

remEm (a :* Empty) = a
remEm (a :~ Empty) = a
remEm (Empty :* a) = a
remEm (Empty :~ a) = a
remEm a = a

rewrites a = do
  liftIO $ print ("reductions",0)
  liftIO $ putStr (pretty a ++ "\n")
  rewritesA 300 a

rewritesA 0 _ = do
  return ()

rewritesA n a = do
  let st0 = collectState a
  case st0 of
    [] -> return ()
    st0 -> put st0
  b <- liftMCheckTimeout (10^3) $ rewrite a
  case b of
    Nothing -> do b <- liftIO $ permute a
                  rewritesA (n-1) b
    Just Nothing     -> rewritesA n a
    (Just (Just  b)) -> do --liftIO $ print (a,st1)
                           st1 <- get
                           let st2 = collectState b
                           let st3 = mergeMcst (st1++st2)
                           put st3
                           liftIO $ print ("reductions",countHides b `div` 2)
                           liftIO $ putStr ("== " ++ pretty b ++ "\n")
                           liftIO $ putStr ("== " ++ pretty (removeHides b) ++ "\n")
                           --liftIO $ print b
                           --liftIO $ print st2
                           rewritesA (n-1) b

countHides (Hide _ _) = 1
countHides (a :* b) = countHides a + countHides b
countHides (a :~ b) = countHides a + countHides b
countHides (I a) = countHides a
countHides _ = 0

maybeToMonad Nothing = mzero
maybeToMonad (Just a) = return a

test01 = T 1 :* T 2 :* I (T 5) :* ( T 8 :~ I (T 2) :~ T 4 )
test02 = T 1 :* I (T 2) :* I (T 5) :* ( T 8 :~ T 2 :~ T 4 )

test03 = ( T 1 :~ T 2 :~ T 3 :~ T 4 ) :* T 5 :* I (T 2) :* ( I (T 1) :~ T 6 :~ T 10 ) :* ( I (T 3) :~ I (T 6) :~ T 7 :~ T 12 ) :* ( I (T 2) :~ I (T 7) :~ T 9 :~ I (T 5) ) 
      :* I ( T 9 ) :* I (T 12 ) :* I (T 4) :* I (T 10) :* ( T 13 :~ T 14 :~ T 15 :~ T 16 :~ T 17 ) :* ( I (T 13) :~ I (T 17 ) ) :* ( I (T 14) :~ I (T 15 ) :~ I (T 16 ) )

test04 = ( T 1 :~ T 2 :~ T 3 ) :* ( I (T 1) :~ I (T 3) :~ I (T 2) )

test05 = ( T 1 :~ T 2 :~ T 3) :* ( I (T 3) :~ T 4 :~ T 5 ) :* ( I (T 1) :~ I (T 5) ) :* ( I (T 4) :~ I (T 2) )

test06 = (Hide 3313280276 (T 1) :~ (T 2 :~ Hide 7703416939 (T 3))) :* ((Hide 7019236527 (I (T 4)) :~ I (T 2)) :* ((Hide 3313280276 (I (T 1)) :~ Hide 9880377035 (I (T 5))) :* (Hide 7703416939 (I (T 3)) :~ (Hide 7019236527 (T 4) :~ Hide 9880377035 (T 5)))))


test07 = OFC ( T 1 :~ I (T 2) :~ I (T 3) :~ I (T 1) ) :* I (T 1) :* T 2 :* T 2 :* T 3 :* T 3 :* OFC ( T 1 )







