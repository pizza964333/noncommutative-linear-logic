{-# LANGUAGE TypeOperators, TypeSynonymInstances, FlexibleInstances #-}

import System.Random
import Control.Applicative
import Control.Monad
import System.IO.Unsafe
import System.Random.Shuffle
import Data.List
import Data.Maybe
import Control.Monad.State.Lazy
import Control.Monad.Trans.Maybe
import System.Timeout
import Data.Functor.Identity

type Marker      = Integer
type Tracker     = Integer
type Generation  = Integer
type CostCounter = Integer

data Term
 = MUnit
 | Invert Term
 | Term `Times` Term
 | Term `Concat` Term
 | Reduced Marker Term
 | Term `Accept` Term
 | Negate Term
 | Favour Term
 | OfCourse (Maybe Tracker) Term
 | SolveOFC Term
 | SolveAUnit Term
 | Focus Term
 | Edge Term Term
 | Atom (Maybe (Tracker,Generation)) String
 deriving (Read,Show,Eq,Ord)

tmap :: Int -> (Term -> Term) -> Term -> Term
tmap n f a = runIdentity $ tmapM n (return.f) a

tmapM :: Monad m => Int -> (Term -> m Term) -> Term -> m Term
tmapM n f (Times    a b) | n > 1 = liftM2 Times       (tmapM n f a) (tmapM n f b)
tmapM n f (Concat   a b) | n > 0 = liftM2 Concat      (tmapM n f a) (tmapM n f b)
tmapM n f (Accept   a b) | n > 1 = liftM2 Accept      (tmapM n f a) (tmapM n f b)
tmapM n f (OfCourse a b) | n > 1 = liftM (OfCourse a) (tmapM n f b)
tmapM n f (Negate     b) | n > 1 = liftM  Negate      (tmapM n f b)
tmapM n f (Invert     b) | n > 2 = liftM  Invert      (tmapM n f b)
tmapM _ f             b  = f b


{-
rotateL (Accept (Times  a b) (Negate c)) = (Accept (Times  c (Invert b)) (Negate a))
rotateL (Accept (Concat a b) (Negate c)) = (Accept (Concat c (Favour b)) (Negate a))
rotateL (Accept (Accept a b) (Negate c)) = (Accept (Accept c (Negate b)) (Negate a))

rotateR (Accept (Concat a b) (Negate c)) = (Accept (Concat (Favour a) c) (Negate b))

rotate  (Accept (OfCourse a) (Negate b)) = (Accept b (Negate (OfCourse a))
-}

data RWState = RWS
 { restorableTerm   :: Term
 , targetTerm       :: Term
 , concatOrderCheck :: [(Marker,([Marker],[Marker]))]
 , costCounter      :: Integer
 , lastMarker       :: Marker
 , lastTracker      :: Tracker
 , lastGeneration   :: Generation
 }
 deriving (Show)

type Rewrite a = StateT RWState (MaybeT IO) a

takeCost :: Integer -> Rewrite ()
takeCost n = state (\st -> ( (), st { costCounter = costCounter st + n } ) )

runRewrite :: (Term -> Rewrite a) -> Term -> IO (Maybe (a,RWState))
runRewrite fun term = runMaybeT (runStateT (fun term) (RWS term MUnit [] 0 0 0 0))

toMaybe :: Rewrite Term -> Rewrite (Maybe Term)
toMaybe a = do
  s <- get
  b <- liftIO $ runMaybeT (runStateT a s)
  case b of
    Nothing    ->          return Nothing
    Just (b,s) -> put s >> return (Just b)

fallback :: Term -> Rewrite Term -> Rewrite Term
fallback a b = do
  c <- toMaybe b
  case c of
    Nothing -> return a
    Just  c -> return c

yield :: Double -> Rewrite a -> Rewrite a
yield a b = do
  c <- liftIO $ randomRIO (0 :: Integer,round (a * 9999999))
  if c < 9999999 then b else mzero

newMarker = do
 st <- get
 let lm = lastMarker st `max` 100000
 a <- liftIO $ randomRIO (1,999)
 let mk = lm+a
 put $ st { lastMarker = mk }
 return mk

newTracker = do
 st <- get
 let lt = lastTracker st `max` 1000
 a <- liftIO $ randomRIO (1,99)
 let tr = lt+a
 put $ st { lastTracker = tr }
 return tr

newGeneration = do
 st <- get
 let lg = lastGeneration st `max` 1
 let gn = lg+1
 put $ st { lastGeneration = gn }
 return gn


trackTerm tr gn term = tmap 2 f term
 where
  f (Atom Nothing a) = Atom (Just (tr,gn)) a
  f               a  = a
  
findEqualTerms = undefined

invert a = tmap 1 f a
 where
  f o@(Atom _ _) = Invert o
  f (Invert o@(Atom _ _)) = o

infixr 8 `Concat`
infixr 7 `Accept`

rewrite :: Term -> Rewrite Term

--rewrite (SolveAUnit (a `Concat` Focus b `Accept` Negate c)) = return $ SolveAUnit $ Invert (Focus b) `Concat` c `Accept` Negate a

rewrite (Reduced _ _) = mzero

rewrite (o1@(Atom a b) `Times` o2@(Invert (Atom c d))) | b == d = do
  m <- newMarker
  return $ Reduced m o1 `Times` Reduced m o2

rewrite o@((r `Concat` u) `Times` (t `Concat` v)) = do
  m <- newMarker
  return $ Reduced m o `Accept` ((r `Times` t) `Concat` (u `Times` v))

rewrite (a `Times`  MUnit) = return a
rewrite (a `Concat` MUnit) = return a
rewrite (Invert     MUnit) = return MUnit
rewrite (OfCourse _ MUnit) = return MUnit

rewrite (Invert (Invert a)) = return a
rewrite (Invert (Atom _ _)) = mzero
rewrite (Invert a) = return $ invert a

rewrite (OfCourse _ o@(OfCourse _ _)) = return o

rewrite (OfCourse Nothing a) = do
  t <- newTracker
  return (OfCourse (Just t) a)

rewrite o@(OfCourse (Just tr) a) = do
  m <- newMarker
  gn <- newGeneration
  return (Reduced m o `Times` trackTerm tr gn a)

rewrite (a@(b `Concat` c) `Times` d) = fun <|> d `rewriteWith` Times a
 where
  fun = do
    (e,f,p) <- randomAtomSelect a
    let (m,_) = p
    g <- findIPF p e d
    if null g then (f `Times` d) `rewriteWith` unhide m
              else do e <- randomElem g
                      return (f `Times` e)

rewrite (a `Times`  b) = foldRew Times  a b
rewrite (a `Concat` b) = foldRew Concat a b

rewrite _ = mzero

rewriteWith a f = liftM f $ rewrite a

randomAtomSelect = undefined

findIPF = undefined

randomElem :: [a] -> Rewrite a
randomElem list = do
  a <- liftIO $ randomRIO (0,b-1)
  return $ list !! a
 where
  b = length list

unhide m a = tmap 2 f a
 where
  f (Reduced m2 b) | m == m2 = b
  f             b            = b

foldRew f a b = do
  c <- toMaybe $ rewrite a
  d <- toMaybe $ rewrite b
  case (c,d) of
    (Nothing,Nothing) -> mzero
    (Just  a,Nothing) -> next f a b
    (Nothing,Just  b) -> next f a b
    (Just  a,Just  b) -> next f a b
 where
  next f a b = do
    let o = f a b
    fallback o $ rewrite o

test01 = Atom Nothing "b" `Times` Invert (Atom Nothing "a") `Times` Invert (Atom Nothing "b")

{-
instance Show (Rewrite Term) where
  show a = unsafePerformIO $ do
   b <- runRewrite (\_ -> a) MUnit
   return $ show b
-}





