{-# LANGUAGE GADTs, TypeOperators, TypeFamilies, FlexibleInstances, FlexibleContexts, MultiParamTypeClasses, ExistentialQuantification, UndecidableInstances #-}
module Data.ProofNet.Types where

import System.Random
import Control.Applicative
import Control.Monad
import System.IO.Unsafe
import System.Random.Shuffle
import Data.List
import Data.Maybe
import Control.Monad.State.Lazy
import Control.Monad.Trans.Maybe
import System.Timeout
import Data.Functor.Identity
import Data.ByteString (ByteString)


data Graph a
data Tree a b

data Term
data Shared
data Rotated
data Rotation a b
data Splitted a
data Focus
data Idle
data Invert a
data Negate a
data Favour a

type EdgeName   = Int
type Tracker    = Int
type AtomName   = Int
type Generation = Int
type Marker     = Int
type Precision  = Int

data a :&: b
infixr :&:

data Fail

type family Reduce a where
 Reduce (Fail :&: a) = Fail
 Reduce (a :&: Fail) = Fail
 Reduce a = a

type family NotShared a where
 NotShared Shared = Fail
 NotShared (Graph a) = Fail
 NotShared (Splitted a) = Fail
 --NotShared (Focus a) = NotShared a
 NotShared (Invert a) = NotShared a
 NotShared (Negate a) = NotShared a
 NotShared (Favour a) = NotShared a
 NotShared (a :&: b) = Reduce (NotShared a :&: NotShared b)

class IsShared a where
instance (NotShared a ~ Fail) => IsShared a where

{-
instance IsShared Shared where
--instance IsShared a => IsShared (Graph a) where
instance IsShared a => IsShared (Focus a) where
instance IsShared a => IsShared (b :&: a) where
instance IsShared a => IsShared (a :&: b) where
-}

class                  GraphElem  a           where
instance               GraphElem (Graph    a) where
instance IsShared a => GraphElem (Tree     a b) where
instance (IsShared a, IsShared b) => GraphElem (Rotation a b) where

infixr 6 `Rotation`
infixr 7 `Accept`
infixr 8 `Times`
infixr 9 `Concat`

data Net a where

 MUnit      ::                                                                 Net (Tree Idle Term)
 ADUnit     ::                                                                 Net (Tree Idle Term)
 ACUnit     ::                                                                 Net (Tree Idle Term)

 OfCourse   ::  Maybe  Tracker             -> Net (Tree a b)                  -> Net (Tree Idle b)
 SolveOFC   ::  Maybe  Tracker             -> Net (Tree a b)                  -> Net (Tree Idle b)

 Atom       ::  Maybe (Tracker,Generation) -> AtomName                      -> Net (Tree Idle Term)
 Reduced    ::  Marker                     -> Net (Tree a b)                  -> Net (Tree Idle b)

 Focus      ::                               Net (Tree a b)                  -> Net (Tree Focus b)
 Rotation   ::  Net (Tree a b)  -> Net (Tree c d) -> Net (Rotation (a,b) (c,d))

 Share      ::  EdgeName                                                    -> Net (Tree Idle Shared)
 Graph      :: (GraphElem a, GraphElem b)  => Net       a   -> Net       b  -> Net (Graph (a :&: b))
 Splitted   :: (IsProofNet a, IsProofNet b) => Net    a -> Net b            -> Net (Splitted (a :&: b))

 Times      ::                                Net (Tree a b)  -> Net (Tree c d) -> Net (Tree Idle (b :&: d))
 Concat     ::               Net (Tree a b)  -> Net (Tree c d) -> Net (Tree Idle (b :&: d))
 Accept     ::                                Net (Tree a b)  -> Net (Tree c d) -> Net (Tree Idle (b :&: d))

 Negate     ::                                Net (Tree a b)                  -> Net (Tree Idle (Negate b))
 Invert     ::                                Net (Tree a b)                  -> Net (Tree Idle (Invert b))
 Favour     ::                 Net (Tree a b)                  -> Net (Tree Idle (Favour b))

 Epsilon    ::                                Net (Tree Idle Term)
 Imaginary  ::                                Net (Tree Idle Term)
 Hyperbolic ::                                Net (Tree Idle Term)

 Natural    ::  Integer                                                     -> Net (Tree Idle Term)
 Bytes      ::  ByteString                                                  -> Net (Tree Idle Term)

 Fractional ::  Precision                  -> Net (Tree a b)                  -> Net (Tree Idle b)


data ProofNet = forall a. IsProofNet a => PFN (Net a)

class IsProofNet a where

instance IsProofNet (Tree a Term) where
instance IsProofNet (Rotation Term Term) where
instance IsProofNet (Graph a) where
instance IsProofNet (Splitted a) where



{-
tmap :: Int -> (Term -> Term) -> Term -> Term
tmap n f a = runIdentity $ tmapM n (return.f) a

tmapM :: Monad m => Int -> (Term -> m Term) -> Term -> m Term
tmapM n f (Times    a b) | n > 1 = liftM2 Times       (tmapM n f a) (tmapM n f b)
tmapM n f (Concat   a b) | n > 0 = liftM2 Concat      (tmapM n f a) (tmapM n f b)
tmapM n f (Accept   a b) | n > 1 = liftM2 Accept      (tmapM n f a) (tmapM n f b)
tmapM n f (OfCourse a b) | n > 1 = liftM (OfCourse a) (tmapM n f b)
tmapM n f (Negate     b) | n > 1 = liftM  Negate      (tmapM n f b)
tmapM n f (Invert     b) | n > 2 = liftM  Invert      (tmapM n f b)
tmapM _ f             b  = f b
-}

{-
data RWState = RWS
 { restorableTerm   :: Net (Tree Rotated)
 , targetTerm       :: Net (Tree Rotated)
 , concatOrderCheck :: [(Marker,([Marker],[Marker]))]
 , costCounter      :: Integer
 , lastMarker       :: Marker
 , lastTracker      :: Tracker
 , lastGeneration   :: Generation
 }

type Rewrite a = StateT RWState (MaybeT IO) a
-}












