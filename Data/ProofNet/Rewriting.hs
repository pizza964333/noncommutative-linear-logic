{-# LANGUAGE TypeOperators, TypeSynonymInstances, FlexibleInstances, GADTs, RankNTypes, ScopedTypeVariables, MultiParamTypeClasses, KindSignatures, FunctionalDependencies #-}
module Data.ProofNet.Rewriting where

import System.Random
import Control.Applicative
import Control.Monad
import System.IO.Unsafe
import System.Random.Shuffle
import Data.List
import Data.Maybe
import Control.Monad.State.Lazy
import Control.Monad.Trans.Maybe
import System.Timeout
import Data.Functor.Identity
import Unsafe.Coerce

import Data.ProofNet.Types
--import Data.ProofNet.Show

class Rewriteable a b | a -> b where
  rewrite :: a -> Rewrite b

instance Rewriteable (Net (Tree (a :&: Focus b))) (Net (Tree (Focus (a :&: Focus b)))) where
 rewrite a@(_ `Concat` (Focus _)) = return $ Focus a
 rewrite a@(_ `Times`  (Focus _)) = return $ Focus a
 rewrite a@(_ `Accept` (Focus _)) = return $ Focus a

instance Rewriteable (Net (Tree (Focus a :&: b))) (Net (Tree (Focus (Focus a :&: b)))) where
 rewrite a@((Focus _) `Concat` _) = return $ Focus a
 rewrite a@((Focus _)  `Times` _) = return $ Focus a
 rewrite a@((Focus _) `Accept` _) = return $ Focus a

instance Rewriteable (Net (Tree (Invert (Focus a)))) (Net (Tree (Focus (Invert (Focus a))))) where
 rewrite a@(Invert  (Focus _))    = return $ Focus a

instance Rewriteable (Net (Tree (Negate (Focus a)))) (Net (Tree (Focus (Negate (Focus a))))) where
 rewrite a@(Negate  (Focus _))    = return $ Focus a

instance Rewriteable (Net (Tree (Focus (Focus a)))) (Net (Tree (Focus a))) where
 rewrite   (Focus a@(Focus _))    = return a

--instance Rewriteable (Net (Rotation (a :&: Focus b) c)) (Net (Rotation (Favour a :&: c) b)) where
-- rewrite (a `Concat` Focus b `Rotation` c) = return $ Favour a `Concat` c `Rotation` b

--instance Rewriteable (Net (Rotation (Focus a :&: b) c)) (Net (Rotation (c :&: Favour b) a)) where
-- rewrite (Focus a `Concat` b `Rotation` c) = return $ c `Concat` Favour b `Rotation` a

instance Rewriteable (Net (Rotation (Focus a :&: Focus b) c)) (Net (Graph (Rotation a (c :&: Shared)) :&: Rotation b Shared)) where
 rewrite (Focus a `Concat` Focus b `Rotation` c) = undefined -- return $ Graph (a `Rotation` c `Concat` Favour (Share 123)) (b `Rotation` Share 123)

{-
data NetRotation = forall a . NR (Net (Rotation a))

instance Rewriteable NetRotation NetRotation where
 rewrite (NR (a `Concat` Focus b `Rotation` c)) = return $ NR $ Favour a `Concat` c `Rotation` b
 rewrite (NR (Focus a `Concat` b `Rotation` c)) = return $ NR $ c `Concat` Favour b `Rotation` a

instance Rewriteable (Net (Rotation a)) (Net Graph) where
 rewrite (Focus a `Concat` Focus b `Rotation` c) = do d <- makeShare; return $ Graph (a `Rotation` c `Concat` Favour (Share d)) (b `Rotation` Share d)

instance Rewriteable ProofNet ProofNet where

 rewrite (PFN a@(Focus _ `Concat` Focus _)) = do b <- rewrite (unsafeCoerce a :: Net (Rotation a)); return $ PFN (b :: Net Graph)

 rewrite (PFN a@(Concat _ _)) = do b <- rewrite (unsafeCoerce a :: Net (Tree Term)); return $ PFN (b :: Net (Tree Term))
 rewrite (PFN a@(Times  _ _)) = do b <- rewrite (unsafeCoerce a :: Net (Tree Term)); return $ PFN (b :: Net (Tree Term))
 rewrite (PFN a@(Accept _ _)) = do b <- rewrite (unsafeCoerce a :: Net (Tree Term)); return $ PFN (b :: Net (Tree Term))
 rewrite (PFN a@(Invert _  )) = do b <- rewrite (unsafeCoerce a :: Net (Tree Term)); return $ PFN (b :: Net (Tree Term))
 rewrite (PFN a@(Negate _  )) = do b <- rewrite (unsafeCoerce a :: Net (Tree Term)); return $ PFN (b :: Net (Tree Term))
 rewrite (PFN a@(Focus  _  )) = do b <- rewrite (unsafeCoerce a :: Net (Tree Term)); return $ PFN (b :: Net (Tree Term))

 rewrite (PFN a@(Rotation _ _)) = do NR b <- rewrite $ NR a; return $ PFN $ (unsafeCoerce b :: Net (Rotation Term))




makeShare = undefined

test01 = PFN $ Atom Nothing 123 
-}




