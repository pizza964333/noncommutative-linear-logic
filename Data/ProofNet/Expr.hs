{-# LANGUAGE GADTs, TypeFamilies, FlexibleInstances #-}

module Data.ProofNet.Expr where

import qualified Data.ProofNet.Types as T
import Data.ProofNet.Types

data Expr
  = MUnit
  | ADUnit
  | ACUnit
  | OfCourse Expr
  | OfCourseT (Maybe Tracker) Expr
  | SolveOFCT (Maybe Tracker) Expr
  | SolveOFC  Expr
  | Atom AtomName
  | AtomG (Maybe (Tracker,Generation)) AtomName
  | Reduced Marker Expr
  | Focus Expr
  | Rotation Expr Expr
  | Share EdgeName
  | Graph  Expr Expr
  | Splitted  Expr Expr
  | Times Expr Expr
  | Concat Expr Expr
  | Accept Expr Expr
  | Negate  Expr
  | Invert Expr
  | Favour Expr Expr
  | Epsilon
  | Imaginary
  | Hyperbolic
  | Natural Integer
  | Bytes String
  | Fractional Precision Expr
 deriving (Show)

infixr 6 `Rotation`
infixr 7 `Accept`
infixr 8 `Times`
infixr 9 `Concat`

