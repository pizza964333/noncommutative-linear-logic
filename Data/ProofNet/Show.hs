{-# LANGUAGE GADTs, TypeFamilies, FlexibleInstances #-}

module Data.ProofNet.Show where

import Data.ProofNet.Types
import qualified Data.ProofNet.Expr as E

instance Show (Net (Tree a)) where
  show a = show $ toExpr a

instance Show (Net (Rotation a)) where
  show a = show $ toExpr a

instance Show ProofNet where
  show a = show $ toExpr a

class ToExpr a where
  toExpr :: a -> E.Expr

class FromExpr a where
  fromExpr :: E.Expr -> a

instance ToExpr (Net a) where

  toExpr MUnit = E.MUnit
  toExpr ADUnit = E.ADUnit
  toExpr ACUnit = E.ACUnit
  toExpr (OfCourse a b) = E.OfCourseT a (toExpr b)
  toExpr (SolveOFC a b) = E.SolveOFCT a (toExpr b)
  toExpr (Atom Nothing b) = E.Atom b
  toExpr (Atom a b) = E.AtomG a b
  toExpr (Reduced a b) = E.Reduced a (toExpr b)
  toExpr (Focus a) = E.Focus (toExpr a)

  toExpr (Rotation a b) = E.Rotation (toExpr a) (toExpr b)
  toExpr (Graph a b) = E.Graph (toExpr a) (toExpr b)
  toExpr (Splitted a b) = E.Splitted (toExpr a) (toExpr b)

  toExpr a = E.MUnit

instance FromExpr (Net (Tree Term)) where

  fromExpr E.MUnit = MUnit
  fromExpr E.ADUnit = ADUnit
  fromExpr E.ACUnit = ACUnit
  fromExpr (E.OfCourseT a b) = OfCourse a (fromExpr b)
  fromExpr (E.SolveOFCT a b) = SolveOFC a (fromExpr b)
  fromExpr (E.Atom b) = Atom Nothing b
  fromExpr (E.AtomG a b) = Atom a b
  fromExpr (E.Reduced a b) = Reduced a (fromExpr b)
  fromExpr (E.Focus a) = Focus (fromExpr a)

instance FromExpr (Net (Rotation Term)) where
  fromExpr (E.Rotation a b) = Rotation (fromExpr a :: Net (Tree Term)) (fromExpr b :: Net (Tree Term))

{-
  toExpr (Graph a b) = E.Graph (toExpr a) (toExpr b)
  toExpr (Splitted a b) = E.Splitted (toExpr a) (toExpr b)
-}

instance ToExpr ProofNet where
  toExpr (PFN a) = toExpr a


instance FromExpr ProofNet where
  fromExpr E.MUnit = PFN MUnit
  fromExpr E.ADUnit = PFN ADUnit
  fromExpr E.ACUnit = PFN ACUnit
  fromExpr (E.OfCourseT a b) = PFN $ OfCourse a (fromExpr b :: Net (Tree Term))
  fromExpr (E.SolveOFCT a b) = PFN $ SolveOFC a (fromExpr b :: Net (Tree Term))
  fromExpr (E.Atom b) = PFN $ Atom Nothing b
  fromExpr (E.AtomG a b) = PFN $ Atom a b
  fromExpr (E.Reduced a b) = PFN $ Reduced a (fromExpr b :: Net (Tree Term))
  fromExpr (E.Focus a) = PFN $ Focus (fromExpr a :: Net (Tree Term))
  fromExpr (E.Rotation a b) = PFN $ Rotation (fromExpr a :: Net (Tree Term)) (fromExpr b :: Net (Tree Term))
  --fromExpr (E.Graph a b) = Graph (fromExpr a) (fromExpr b)




